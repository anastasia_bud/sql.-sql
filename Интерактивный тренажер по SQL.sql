--2.4 Запросы на основе трех и более связанных таблиц
      -- Вывести все заказы Баранова Павла (какие книги, по какой цене и в каком количестве он заказал) в отсортированном по номеру
      -- заказа и названиям книг виде.

SELECT buy.buy_id, book.title, book.price, buy_book.amount
FROM client INNER JOIN buy
     USING(client_id)
            INNER JOIN buy_book
     USING(buy_id)
            INNER JOIN book
     USING(book_id)
WHERE name_client='Баранов Павел'
ORDER BY  buy.buy_id, book.title;

--2.4. Посчитать, сколько раз была заказана каждая книга, для книги вывести ее автора (нужно посчитать, в каком количестве заказов 
       -- фигурирует каждая книга). Результат отсортировать сначала  по фамилиям авторов, а потом по названиям книг. 

SELECT name_author, title, COUNT(buy_book.buy_id) AS Количество
FROM author INNER JOIN book USING(author_id)
            LEFT JOIN buy_book USING(book_id)
GROUP BY name_author, title
ORDER BY name_author, title;

--2.4. Вывести города, в которых живут клиенты, оформлявшие заказы в интернет-магазине. Указать количество заказов в каждый город.
       -- Информацию вывести по убыванию количества заказов, а затем в алфавитном порядке по названию городов.

SELECT name_city, COUNT(buy_id) AS Количество
FROM city INNER JOIN client USING(city_id)
          INNER JOIN buy USING(client_id)
GROUP BY name_city
ORDER BY Количество DESC, name_city;

--2.4 Вывести номера всех оплаченных заказов и даты, когда они были оплачены.

SELECT buy_id, date_step_end
FROM step INNER JOIN buy_step USING(step_id)
WHERE name_step='Оплата' AND date_step_end IS NOT NULL;

--2.4 Вывести информацию о каждом заказе: его номер, кто его сформировал (фамилия пользователя) и его стоимость (сумма
      -- произведений количества заказанных книг и их цены), в отсортированном по номеру заказа виде.

SELECT buy.buy_id, name_client, SUM(price*buy_book.amount) AS Стоимость
FROM client INNER JOIN buy USING(client_id)
            INNER JOIN buy_book USING(buy_id)
            INNER JOIN book USING(book_id)
GROUP BY buy.buy_id, name_client
ORDER BY buy.buy_id;

--2.4 Вывести все заказы и названия этапов, на которых они в данный момент находятся. Если заказ доставлен –  информацию о нем не  
    -- выводить. Информацию отсортировать по возрастанию buy_id.

SELECT buy_id, name_step
FROM step INNER JOIN buy_step USING(step_id)
WHERE date_step_beg IS NOT NULL AND date_step_end IS NULL
ORDER BY buy_id;

--2.4 В таблице city для каждого города указано количество дней, за которые заказ может быть доставлен в этот город 
      -- (рассматривается только этап "Транспортировка"). 
      -- Для тех заказов, которые прошли этап транспортировки, вывести количество дней за которое заказ реально доставлен в город. 
      -- А также, если заказ доставлен с опозданием, указать количество дней задержки, в противном случае вывести 0. Информацию 
      -- вывести в отсортированном по номеру заказа виде.

SELECT buy.buy_id, 
       DATEDIFF(date_step_end,date_step_beg) AS Количество_дней, 
       IF(DATEDIFF(date_step_end,date_step_beg)>days_delivery, DATEDIFF(date_step_end,date_step_beg)-days_delivery, 0) AS Опоздание
FROM city INNER JOIN client USING(city_id)
          INNER JOIN buy USING(client_id)
          INNER JOIN buy_step USING(buy_id)
          INNER JOIN step USING(step_id)
WHERE name_step='Транспортировка' AND date_step_end IS NOT NULL
ORDER BY buy.buy_id

--2.4 Выбрать всех клиентов, которые заказывали книги Достоевского, информацию вывести в отсортированном по алфавиту виде.

SELECT name_client
FROM author INNER JOIN book USING(author_id)
            INNER JOIN buy_book USING(book_id)
            INNER JOIN buy USING(buy_id)
            INNER JOIN client USING(client_id)
WHERE name_author='Достоевский Ф.М.'
ORDER BY name_client;

--2.4 Вывести жанр (или жанры), в котором было заказано больше всего экземпляров книг, указать это количество .

SELECT name_genre, SUM(buy_book.amount) AS Количество
FROM genre INNER JOIN book USING(genre_id)
           INNER JOIN buy_book USING(book_id)
GROUP BY name_genre
HAVING SUM(buy_book.amount)=(SELECT MAX(Количество) AS max_Количество
                             FROM (SELECT genre_id, SUM(buy_book.amount) AS Количество
                                   FROM buy_book INNER JOIN book USING(book_id)
                                   GROUP BY genre_id) AS query_1);


--2.4 Сравнить ежемесячную выручку от продажи книг за текущий и предыдущий годы. Для этого вывести год, месяц, сумму выручки в 
      -- отсортированном сначала по возрастанию месяцев, затем по возрастанию лет виде. Название столбцов: Год, Месяц, Сумма.

SELECT YEAR(date_payment) AS Год, MONTHNAME(date_payment) AS Месяц, SUM(price*amount) AS Сумма
FROM buy_archive
GROUP BY YEAR(date_payment), MONTHNAME(date_payment)
UNION ALL
SELECT YEAR(date_step_end) AS Год, MONTHNAME(date_step_end) AS Месяц, SUM(book.price*buy_book.amount) AS Сумма
FROM book INNER JOIN buy_book USING(book_id)
          INNER JOIN buy USING(buy_id)
          INNER JOIN buy_step USING(buy_id)
          INNER JOIN step USING(step_id)
WHERE name_step='Оплата' AND date_step_end IS NOT NULL
GROUP BY YEAR(date_step_end), MONTHNAME(date_step_end)
ORDER BY Месяц, Год

--2.4 Вычислить общее количество проданных книг (столбец Количество) и их стоимость (столбец Сумма) за текущий и предыдущий год. 
      -- Информацию отсортировать по убыванию стоимости.

SELECT title, SUM(Количество) AS Количество, SUM(Сумма) AS Сумма
FROM (SELECT title, SUM(buy_archive.amount) AS Количество, SUM(buy_archive.amount*buy_archive.price) AS Сумма
      FROM buy_archive INNER JOIN book USING(book_id)
      GROUP BY title
      UNION
      SELECT title, SUM(buy_book.amount) AS Количество, SUM(buy_book.amount*book.price) AS Сумма
      FROM book INNER JOIN buy_book USING(book_id)
                INNER JOIN buy USING(buy_id)
                INNER JOIN buy_step USING(buy_id)
                INNER JOIN step 
                      ON (buy_step.step_id, 'Оплата')=(step.step_id, step.name_step) 
                          AND date_step_end IS NOT NULL
      GROUP BY title) AS query_1
GROUP BY title
ORDER BY Сумма DESC;
	  
--2.5 Включить нового человека в таблицу с клиентами. Его имя Попов Илья, его email popov@test, проживает он в Москве.

INSERT INTO client (name_client, city_id, email)
            SELECT 'Попов Илья', city_id, 'popov@test'
            FROM city
            WHERE name_city='Москва';

--2.5 Создать новый заказ для Попова Ильи. Его комментарий для заказа: «Связаться со мной по вопросу доставки».

INSERT INTO buy(buy_description, client_id)
            SELECT 'Связаться со мной по вопросу доставки', client_id
            FROM client
            WHERE name_client='Попов Илья';
			
--2.5 В таблицу buy_book добавить заказ с номером 5. Этот заказ должен содержать книгу Пастернака «Лирика» в количестве двух    
      -- экземпляров и книгу Булгакова «Белая гвардия» в одном экземпляре.

INSERT INTO buy_book(buy_id, book_id, amount)
            SELECT '5', (SELECT book_id
                         FROM author INNER JOIN book USING(author_id)
                         WHERE name_author='Пастернак Б.Л.' 
                               AND title='Лирика'),
                   '2';
INSERT INTO buy_book(buy_id, book_id, amount)
            SELECT '5', (SELECT book_id
                         FROM author INNER JOIN book USING(author_id)
                         WHERE name_author='Булгаков М.А.' 
                               AND title='Белая гвардия'),
                   '1'; 

--2.5 Количество тех книг на складе, которые были включены в заказ с номером 5, уменьшить на то количество, которое в заказе с 
      -- номером 5  указано.

UPDATE book INNER JOIN buy_book USING(book_id)
SET book.amount=book.amount-buy_book.amount
WHERE buy_book.buy_id='5';

--2.5 Создать счет (таблицу buy_pay) на оплату заказа с номером 5, в который включить название книг, их автора, цену, количество   
      -- заказанных книг и  стоимость. Информацию вывести в отсортированном по названиям книг виде.

CREATE TABLE buy_pay AS
             SELECT title, name_author, price, buy_book.amount, price*buy_book.amount AS Стоимость
             FROM author JOIN book USING(author_id)
                         JOIN buy_book USING(book_id)
             WHERE buy_id='5'
             ORDER BY title;

--2.5 Создать общий счет (таблицу buy_pay) на оплату заказа с номером 5. Куда включить номер заказа, количество книг в заказе и 
      -- его общую стоимость. Для решения используйте ОДИН запрос.

CREATE TABLE buy_pay AS 
             SELECT buy_id, SUM(buy_book.amount) AS Количество, SUM(price*buy_book.amount) AS Итого
             FROM book JOIN buy_book USING(book_id)
             WHERE buy_id='5'
             GROUP BY buy_id;
SELECT * FROM buy_pay;

--2.5 В таблицу buy_step для заказа с номером 5 включить все этапы из таблицы step, которые должен пройти этот заказ. В столбцы 
      -- date_step_beg и date_step_end всех записей занести Null.

INSERT INTO buy_step (buy_id, step_id)
            SELECT buy_id, step_id
            FROM buy CROSS JOIN step
            WHERE buy_id='5';
			
--2.5 В таблицу buy_step занести дату 12.04.2020 выставления счета на оплату заказа с номером 5. Правильнее было бы занести не 
      -- конкретную, а текущую дату. Это можно сделать с помощью функции Now(). Но при этом в разные дни будут вставляться разная 
      -- дата, и задание нельзя будет проверить, поэтому  вставим дату 12.04.2020.	

UPDATE buy_step 
      INNER JOIN step
      ON buy_step.step_id = step.step_id
SET date_step_beg = '2020-04-12'
WHERE name_step ='Оплата' AND buy_id = 5;

--2.5 Завершить этап «Оплата» для заказа с номером 5, вставив в столбец date_step_end дату 13.04.2020, и начать следующий этап («Упаковка»), задав в столбце date_step_beg для этого этапа ту же дату.Реализовать два запроса для завершения этапа и начала следующего. Они должны быть записаны в общем виде, чтобы его можно было применять для любых этапов, изменив только текущий этап. Для примера пусть это будет этап «Оплата».			

UPDATE buy_step JOIN step USING(step_id)
SET date_step_end='2020-04-13'
WHERE buy_id=5 AND name_step='Оплата';

UPDATE buy_step JOIN step USING(step_id)
SET date_step_beg='2020-04-13'
WHERE buy_id=5 AND name_step='Упаковка';
               
SELECT * FROM buy_step
WHERE buy_id=5;

--2.5 творч.задание. Удалить из базы данных (таблицы buy, buy_step, buy_book) сведения о заказах, оплата которых не завершилась в течение суток с даты размещения заказа + добавить, что перед удалением информации из таблицы buy_book следует перенести количество зарезервированных под заказ книг (поле amount таблицы buy_book) обратно на склад (поле amount таблицы book)

UPDATE book JOIN buy_book USING(book_id)
SET book.amount=book.amount+buy_book.amount
WHERE buy_id=(SELECT buy.buy_id
              FROM buy_book JOIN buy USING(buy_id)
                            JOIN buy_step USING(buy_id)
                            JOIN step USING(step_id)
              WHERE name_step='Оплата' AND 
                    date_step_end IS NULL);
DELETE FROM buy
WHERE buy_id=(SELECT buy_step.buy_id
              FROM buy_step JOIN step USING(step_id)
              WHERE name_step='Оплата' AND 
                    date_step_end IS NULL);
                    
SELECT * FROM buy;
SELECT * FROM buy_step;
SELECT * FROM buy_book;
SELECT * FROM book;

--3.1 Вывести студентов, которые сдавали дисциплину «Основы баз данных», указать дату попытки и результат. Информацию вывести по 
      -- убыванию результатов тестирования.

SELECT name_student, date_attempt, result
FROM student JOIN attempt USING(student_id)
             JOIN subject USING (subject_id)
WHERE name_subject='Основы баз данных'
ORDER BY result DESC;

--3.1 Вывести, сколько попыток сделали студенты по каждой дисциплине, а также средний результат попыток, который округлить до 2 
      -- знаков после запятой. Под результатом попытки понимается процент правильных ответов на вопросы теста, который занесен в 
      -- столбец result. Информацию вывести по убыванию средних результатов.

SELECT name_subject, COUNT(attempt.subject_id) AS Количество, round(AVG(result), 2) AS Среднее
FROM subject LEFT JOIN attempt USING(subject_id)
GROUP BY name_subject
ORDER BY Среднее DESC;

--3.1 Вывести студента (различных студентов), имеющих максимальные результаты попыток. Информацию отсортировать в алфавитном 
      -- порядке по фамилии студента. Максимальный результат не обязательно будет 100%, поэтому явно это значение в запросе не 
      -- задавать.

SELECT DISTINCT name_student, result
FROM student JOIN attempt USING(student_id)
WHERE result=(SELECT MAX(result) AS max_result
              FROM attempt)
ORDER BY name_student;

--3.1 Если студент совершал несколько попыток по одной и той же дисциплине, то вывести разницу в днях между первой и последней 
    -- попыткой. Информацию вывести по возрастанию разницы. Студентов, сделавших одну попытку по дисциплине, не учитывать. 

SELECT name_student, name_subject, 
       DATEDIFF(MAX(date_attempt), MIN(date_attempt)) AS Интервал
FROM student JOIN attempt USING(student_id)
             JOIN subject USING(subject_id)
GROUP BY name_student, name_subject
HAVING COUNT(date_attempt)>1
ORDER BY Интервал

--3.1 Для каждой дисциплины вывести количество студентов, которые проходили по ней тестирование. Информацию отсортировать сначала 
      -- по убыванию количества, а потом по названию дисциплины. В результат включить и дисциплины, тестирование по которым студенты
       -- еще не проходили, в этом случае указать количество студентов 0.

SELECT name_subject, COUNT(DISTINCT student_id) AS Количество
FROM subject LEFT JOIN attempt USING(subject_id)
GROUP BY name_subject
ORDER BY name_subject

--3.1 Случайным образом отберите 3 вопроса по дисциплине «Основы баз данных».

SELECT question_id, name_question
FROM subject JOIN question USING(subject_id)
WHERE name_subject='Основы баз данных'
ORDER BY RAND() LIMIT 3;

--3.1 Вывести вопросы, которые были включены в тест для Семенова Ивана по дисциплине «Основы SQL» 2020-05-17 (значение attempt_id 
      -- для этой попытки равно 7). Указать, какой ответ дал студент и правильный он или нет. 

SELECT name_question, name_answer, IF(is_correct=true, 'Верно', 'Неверно') AS Результат
FROM question JOIN testing USING(question_id)
              JOIN answer USING(answer_id)
WHERE attempt_id=7

--3.1 Посчитать результаты тестирования. Результат попытки вычислить как количество правильных ответов, деленное на 3 (количество 
      -- вопросов в каждой попытке) и умноженное на 100. Результат округлить до двух знаков после запятой. Вывести фамилию студента, 
      -- название предмета, дату и результат. Информацию отсортировать сначала по фамилии студента, потом по убыванию даты попытки.

SELECT name_student, name_subject, date_attempt, round(SUM(is_correct)/3*100, 2) AS Результат
FROM student JOIN attempt USING(student_id)
             JOIN subject USING(subject_id)
             JOIN testing USING(attempt_id)
             JOIN answer USING(answer_id)
GROUP BY name_student, name_subject, date_attempt
ORDER BY name_student, date_attempt DESC;   

--3.1 Для каждого вопроса вывести процент успешных решений, то есть отношение количества верных ответов к общему количеству 
      -- ответов, значение округлить до 2-х знаков после запятой. Также вывести название предмета, к которому относится вопрос, и 
      -- общее количество ответов на этот вопрос. Информацию отсортировать сначала по названию дисциплины, потом по убыванию 
      -- успешности, а потом по тексту вопроса в алфавитном порядке. Поскольку тексты вопросов могут быть длинными, обрезать их 
      -- 30 символов и добавить многоточие "...".

SELECT name_subject, CONCAT(LEFT(name_question, 30), '...') AS Вопрос, 
       COUNT(testing.answer_id) AS Всего_ответов,
       round((sum(is_correct)/COUNT(testing.answer_id)*100), 2) AS Успешность
FROM testing JOIN answer USING(answer_id)
             JOIN question ON answer.question_id=question.question_id
             JOIN subject USING(subject_id)
GROUP BY name_subject, name_question
ORDER BY name_subject, Успешность DESC, name_question;

--3.1 Баранов хочет посмотреть, где он ошибся, а где - нет. Создать таблицу baranov. Включить название дисциплины, дату попытки,
      -- вопрос (ограниченный до 20 символов), ответ Баранова, результат (Верно/Неверно). Отсортировать по названию дисциплины, дате  
      -- попытки, вопросу по алфавиту. Составить запрос так, чтобы когда придут другие студенты, достаточно было бы заменить фамилию

SELECT name_subject, date_attempt, LEFT(name_question, 15) AS Вопрос, LEFT(name_answer, 35) AS Ответ,
       IF(is_correct, 'Верно', 'Неверно') AS Результат
FROM attempt a JOIN student s USING(student_id)
             JOIN testing t USING(attempt_id)
             JOIN question q  ON q.question_id = t.question_id
             JOIN answer ans ON ans.answer_id = t.answer_id
             JOIN subject sub ON sub.subject_id = q.subject_id               
WHERE name_student='Баранов Павел'
ORDER BY name_subject, date_attempt, name_question;

--3.2 творческое задание. Выбрать те вопросы, где студенты допустили одну и ту же ошибку наибольшее количество раз. Вывести ответ 
      -- и количество раз, сколько выбрали этот ответ. (Исходя из таблицы testing это ответ 16, который является неверным, его 
      -- ученики выбрали два раза).
      
SELECT LEFT(name_question, 35), LEFT(name_answer,20), COUNT(is_correct) AS Количество_ошибок 
FROM question JOIN testing ON question.question_id=testing.question_id
              JOIN answer ON answer.answer_id=testing.answer_id AND answer.is_correct=false
GROUP BY LEFT(name_question, 35), LEFT(name_answer,20)
HAVING COUNT(is_correct)=(SELECT MAX(Количество_ошибок)
                          FROM (SELECT testing.answer_id, COUNT(is_correct) AS Количество_ошибок
                                FROM testing JOIN answer ON answer.answer_id=testing.answer_id 
                                                            AND answer.is_correct=false
                                GROUP BY testing.answer_id) AS query_1);  